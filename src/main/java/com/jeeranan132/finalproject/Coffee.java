/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.jeeranan132.finalproject;

/**
 *
 * @author ASUS
 */
public class Coffee {

    private String menu;
    private int quantity;
    private int price;

    public Coffee(String menu, int quantity, int price) {
        this.menu = menu;
        this.quantity = quantity;
        this.price = price;
    }

    public String getMenu() {
        return menu;
    }
    
    public int getQuantity() {
        return quantity;
    }
    
    public int getPrice() {
        return price;
    }
    
    public void setMenu(String menu) {
        this.menu = menu;
    }
    
    public void setNum(int quantity) {
        this.quantity = quantity;
    }
    
    public void setPrice(int price) {
        this.price = price;
    }
    
    @Override
    public String toString() {
        return menu + "     QTY." + quantity + "       " + price + "  Bath. "+"\n";
    }
      
}
